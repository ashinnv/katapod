 
#![allow(dead_code)]

//(time speech chunk starts, time speech chunk ends, what was said)
pub struct ScriptSlice(u64,u64,String);

//Metadata for the episode
pub struct Meta{
  pub file_name: String,            //not necessarily the episode title
  pub playcnt:   u64,               //Play count
  pub url:       String,            //URL the file was downloaded from
  pub ag_rating: (f64, u64),        //Aggregated ratings (aggregated rating, number of ratings)-- Number of rats is for averaging
  pub lst_rating: i64,              //Last rating given
  pub description: String,          //The typical episode description you get under a podcast episode
  pub shownotes: String,            //Episode shownotes
  pub thumbnail: Vec<u8>,           //Binary data of show's thumbnail
  pub lst_play:  u64,                //Unix timestamp of last play stopped
  pub lft_off:   u64,                //Seconds along playback that the last leftoff place was
  pub chapters:  Vec<(String, u64)>, //Vector of timestamps and titles of each chapter
  pub guests:    Vec<String>,        //Guests on this episode
} 

impl Meta{
  pub fn make_blank() -> Meta {
    let ch_vec: Vec< (String,u64) > = vec![];
    let gsts: Vec<String> = vec![];
    let tn: Vec<u8> = vec![];

    Meta{
      file_name: "".to_string(),
      playcnt: 0,
      url: "".to_string(),
      ag_rating: (0.0,0),
      lst_rating: 0,
      description: "".to_string(),
      shownotes: "".to_string(),
      thumbnail: tn,
      lst_play: 0,
      lft_off: 0,
      chapters: ch_vec,
      guests: gsts,
    }
  }
}

//Individual episodes of a podcast
pub struct Episode{
  pub data: Vec<u8>,                //File serial binary data 
  pub extension: String,            //File extension
  pub meta: Meta,                   //Metadata 
  pub script: Vec<(String, Vec<ScriptSlice>)>,//Scripts (transcriptions) as a vector of (Language name, Vec<chunk of speech>)
  pub title: String
}

impl Episode{

  pub fn make_blank() -> Episode{

    let met: Meta = Meta::make_blank();
    let dead_data: Vec<u8> = vec![];
    let dead_vec: Vec<(String, Vec<ScriptSlice>)> = vec![];

    Episode{
      data: dead_data,
      extension: "".to_string(),
      meta: met,
      script: dead_vec,
      title: "".to_string(),
    }
  }


}

//Different shows
pub struct Show{
  pub title: String,
  pub carrier: String,              //Podcasting or video provider, such as youtube, rumble, apple podcasts, Google podcasts, Internet Archive, etc...  
  pub hosts: Vec<String>,           //List of names of the people responsible for this podcast
  pub description: String,          //Show description,vs episode description
  pub episodes: Vec<Show>,
}

impl Show{

  pub fn make_blank() -> Show{


    Show{
      title: "".to_string(),
      carrier: "".to_string(),
      hosts: vec![],
      description: "".to_string(),
      episodes: vec![],
    }
  }

}


impl Meta{
  pub fn set_file_name(&mut self, fname: String){
    self.file_name = fname;
  }
  pub fn set_play_count(&mut self, pcount: u64){
    self.playcnt = pcount;
  }
  pub fn iter_play_count(&mut self){
    self.playcnt += 1;
  }
  pub fn set_url(&mut self, new_url:String){
    self.url = new_url;
  }
  pub fn set_ag(&mut self, set_no: u64){
    let (mut a,b) = self.ag_rating;
    a = set_no as f64;
    self.ag_rating = (a,b);
  }
  //Update the average rating with a new one that includes the new rating. Reminds me way too much of the old "NEW AVERAGING SYSTEM" meme that went around a few years ago: https://pastebin.com/qqWUpcxa
  pub fn append_ag_rat(&mut self, new_rat: u64){
    let (ar, n) = self.ag_rating;
    let ar_times = ar * n as f64;
    let new_avg = (ar_times + new_rat as f64) / (n+1) as f64;
  }

}