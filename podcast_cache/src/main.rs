/*
  TOD: More error handling.
*/

use mysql::*;
use mysql::prelude::*;

use std::fs;
use std::str;
use std::fs::File;
use std::time::SystemTime;
use std::fs::OpenOptions;
use std::io::Write;


use opml::OPML;
use curl::easy::Easy;

mod strdef;

extern crate redis;

fn main() {

  let tmp = strdef::Meta::make_blank();
  

  log("Starting\n");

  //TODO: optional file locations. File searching?
  //Get the podcasts from the opml file
  let mut fdat = String::from("");
  match fs::read_to_string("../apod_full.opml"){
    Ok(d)=>fdat = d,
    Err(e)=>println!("Error reading opml: {:?}", e),
  };

  //TODO: Look into creating the type var a better way
  //let mut opdat: opml::OPML;
  match OPML::from_str(&fdat){
    Ok(od)=>{
      let opdat = od;


        //Iterate over the links to rss feeds in the opml data
        for line in opdat.body.outlines{
          let xml_dat = line.xml_url.unwrap();
          let spl: Vec<&str> = xml_dat.split("\n").collect();
          for url in spl{

            let mut easy = Easy::new();
            easy.url(url).unwrap();

            //Download the rss feed xml (I HATE RUST CLOSURES SO MUCH)
            match easy.write_function(|data| {
              let mut s = String::from("");
                //this is the text of that podcast's rss feed
                //let s:String = String::from_utf8(data.to_vec()).unwrap();
                match String::from_utf8(data.to_vec()){
                  Ok(d)=>s=d,
                  Err(e)=>log(&format!("Error in main, ln. 43: {}", e)),
                };
                      
                //gen_file(&s).unwrap();//Write full dump of xml file to a backup
                let lines: Vec<&str> = s.split("\n").collect();
                for line in lines{
                  let (url,check) = get_url(line.to_string());
                  if check{
                    println!("Fin: {}", url);
                    match download_link(url){
                      Ok(_)=>{},
                      Err(e)=>{
                        println!("Error in download_link function: {:?}", e)
                      },
                    };
                  }else{
                    if url == "bad"{
                      //rerun the pull
                    }
                  }
                }

                Ok(data.len())
                }){
                  Ok(_)=>log("good download"),
                  Err(e)=>log(&format!("Error with easy.write_functiion: {}", e)),
                };

                match easy.perform()
                {
                  Ok(_)=>continue,
                  Err(e)=>log(&format!("Failed in main {}\n", e)),
                };
          }
        }
          },
          
            Err(e)=>println!("Error with creating opml struct: {:?}", e),
        };

}

//Get the link to the audio file from the xml
fn get_url(input: String) -> (String, bool){

    let spl: Vec<&str> = input.split("\n").collect();
    //println!("Length of vec: {}", spl.len());

    //Find the line with media link
    for line in spl{
        let linkdat = line.to_string();
        if is_audio_link(linkdat.to_string()){
            if check_quotes_closed(&linkdat){ //sad attempt at validating data
                return(line.to_string(), true);
            }else{
                return("bad".to_string(), false);
            }
        }
    }

    //Couldn't find media file link
    (String::from(""), false)
    
}

//Check if url links to an audio file
fn is_audio_link(input: String) -> bool{
    let f_names: Vec<&str> = vec![".mp3", ".m4a", ".ogg", "flac"];

    for name in f_names{
        if input.contains(name){
            //println!("Found One");
            return true
        }
    }

    false
}

fn _gen_file(input: &str) -> std::io::Result<()>{

    //let mut file = File::create("full_dump.xml")?;
    //let mut file = File::open("full_dump.xml")?;
    let mut file = File::options().append(true).open("full_dump.xml")?;
    file.write_all(input.as_bytes())?;
    Ok(())

}

//are all quotes closed?
fn check_quotes_closed(input: &str) -> bool{
    if input.matches("\"").count() % 2 == 0{
        return true
    }

    false
    
}

//Download an "<enclosure url=" style link"
fn download_link(nput: String) -> std::result::Result<(), curl::Error>{

  let client = redis::Client::open("redis://localhost").unwrap();
  //let con = client.get_connection().unwrap();
    
    
  let mut dst = Vec::new();
  let mut easy = Easy::new();

  let mut input = nput.replace("<enclosure url=\"","");
  input = input.replace("\" length=\"0\" type=\"audio/mpeg\" />", "");

  //println!("Opening {}", input);
  match easy.url(&input){
    Ok(_)=>{},
    Err(e)=>{
      println!("Error in easy url: {:?}",e);
    },
  };

  let _redirect = easy.follow_location(true);
    
  {
    let mut trans = easy.transfer();
      match trans.write_function(|data|{
        dst.extend_from_slice(data);
        Ok(data.len())
      }){
      //Ok(r)=> println!("Worked: {:?}", r),
        Ok(_)=> {},
        Err(e)=>{
          println!("Failed: {}", e);
          log(&format!("Failed in download_link: {}\n", e));
        }
      };
      trans.perform()?;
    }//<-Why?
    {//Why?
        
      
    /*let tnow =*/
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH){
      Ok(tnow)=>{
        let t_str: String= format!("{:?}", tnow);
        println!("TSTR: {}", &t_str);
      }
      Err(e)=>println!("Error getting current time: {:?}", e)
    };

    //let t_str: String = tnow.to_string();
    //let t_str: String = format!("{:?}", tnow);
        
    //let _:() = con.set(t_str+".mp3",dst.as_slice()).unwrap();
    //println!("TSTR: {}", &t_str);
    
    //let mut file = File::create(&t_str.to_string()+".mp3".to_string()).unwrap();
    //file.write_all(dst.as_slice()).unwrap();
  }
  Ok(())
}

//Lumberjackss
fn log(input: &str) {
    let mut file_ref = OpenOptions::new().append(true).open("log.txt").expect("Unable to open file");   
    file_ref.write_all(input.as_bytes()).expect("write failed");
    
}





























































        //println!("{}", line.xml_url.unwrap());
        /*
            pub struct Outline { 

                pub text: String,
                pub type: Option<String>,
                pub is_comment: Option<bool>,
                pub is_breakpoint: Option<bool>,
                pub created: Option<String>,
                pub category: Option<String>,
                pub outlines: Vec<Outline>,
                pub xml_url: Option<String>, <-- Link to the RSS feed itself, which contains the MP3 link
                pub description: Option<String>,
                pub html_url: Option<String>,
                pub language: Option<String>,
                pub title: Option<String>,
                pub version: Option<String>,
                pub url: Option<String>,
            }
        */